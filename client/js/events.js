"use strict";

Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();
        var postName = event.currentTarget.children[0].children[0].value,
            postMessage = event.currentTarget.children[0].children[1].value,
            postImage = event.currentTarget.children[0].children[2].files[0];


        Collections.Images.insert(postImage, function (error, fileObject) {
            if (!postName.trim() || !postMessage.trim()) {
                alert("Can't leave post blank!");
            } else {    
                Collections.Post.insert({
                    author: postName,
                    message: postMessage,
                    image_id: fileObject._id,
                    createdAt: new Date(),
                    
                });
            }
            $(".grid").masonry("reloadItems");
        });

        event.currentTarget.children[0].children[0].value = "";
        event.currentTarget.children[0].children[1].value = "";
        event.currentTarget.children[0].children[2].value = "";   
        
        return false;
    }
});