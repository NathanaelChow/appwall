Template.posts.helpers({
    imageList: function () {
        return Collections.Images.find({}, {
            sort: {
                createdAt: -1
            }
        });

    },

    postList: function () {
        return Collections.Post.find({}, {
            sort: {
                createdAt: -1
            }
        });
    },
    imageDisplay: function (fileObject) {
        var image = Collections.Images.find({
            _id: this.image_id
        }).fetch();
        return image[0].url();
    },


    updateMasonry: function () {
        $(".grid").imagesLoaded().done(function () {
            $(".grid").masonry({
                itemSelector: ".grid-item",
                columnWidth: ".grid-sizer",
                percentPosition: true,
                gutter: 15
            });
        });
    }

});
